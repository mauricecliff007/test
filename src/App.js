
import './App.css';
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import { decrement, increaseAmount, increment } from './redux/slices/createSlice';
import { fetchPosts } from './redux/slices/postsSlice';
import { useEffect } from 'react';




function App() {
    const counter = useSelector((state) => state?.counter);
    console.log(counter);
    const dispatch = useDispatch()
    useEffect(() => {
      dispatch(fetchPosts())
    }, [dispatch])

    const posts = useSelector((state) => state.posts)
    console.log(posts)
    const { loading, error, postsList } = posts
    
  return (
    <div className="App">
      <h1>Redux Tool Kit See Through</h1>
      <h1>Counter: {counter?.value}</h1>
      <button onClick={()=> dispatch(decrement())}>-</button>
      <button onClick={()=> dispatch(increment())}>+</button>
      <button onClick={()=> dispatch(increaseAmount(20))}>increaseAmount</button>
      <div className="post">
        <h1>Posts-List</h1>
        {postsList && (
          <div className="postsList">
           {loading ? 
             <p>loading postlist</p> : error ? 
             <p>{error.message}</p> 
              : postsList.slice(0, 20).map(post => (
                 <ul key={post.id}>
                   <hr />
                   <h3>{post.title}</h3>
                   <p>{post.body}</p>
                 </ul>
              ))
             }
          </div>
        )}
      </div>
    </div>
  );
}

export default App;
