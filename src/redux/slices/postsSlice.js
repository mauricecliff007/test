import {createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';

//action
export const fetchPosts = createAsyncThunk('posts/List', async(payload, 
    {rejectWithValue, getState, dispatch}) => {
  try {
    const { data } = await axios.get('https://jsonplaceholder.typicode.com/posts');
    return data
  }catch(error){
    return error?.response
  }
})


//reducer
export const postSlices = createSlice({
   name: 'posts',
   initialState: {},
   extraReducers: {
       //handling pending state
      [fetchPosts.pending]: (state, action) => {
         state.loading = true;
      },

      [fetchPosts.fulfilled]: (state, action) => {
          state.loading = false;
          state.postsList = action.payload
      },

      [fetchPosts.rejected]: (state, action) => {
          state.loading = false;
          state.error = action.payload;
      }
   }

});


export default postSlices.reducer;