import { configureStore } from '@reduxjs/toolkit';
import counterSlices from '../slices/createSlice';
import postSlices  from '../slices/postsSlice';
// import { counterSlices } from '../slices/counterSlices';


const store = configureStore({
    reducer: {
        counter: counterSlices,
        posts: postSlices
    }
})



export default store;


